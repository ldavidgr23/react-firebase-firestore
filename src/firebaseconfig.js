import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCO56yjgQsdt7iq3o7n3wiSTjC0E7m6vo0",
    authDomain: "react-firestore-312cc.firebaseapp.com",
    projectId: "react-firestore-312cc",
    storageBucket: "react-firestore-312cc.appspot.com",
    messagingSenderId: "391589450873",
    appId: "1:391589450873:web:41efe7b2c8b4075014b0a8"
  };
  // Initialize Firebase
  const fireb = firebase.initializeApp(firebaseConfig);
  const store = fireb.firestore()
  
  export {store}