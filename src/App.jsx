import {store} from './firebaseconfig'
import React,{useState, useEffect} from 'react'

function App() {
    const [modoedicion, setModoEdicion] = useState(null)
    const [idusuario, setIdUsuario] = useState('')
    const [phone, setPhone] = useState('')
    const [nombre, setNombre] = useState('')
    const [usuariosAgenda, setUsuariosAgenda] = useState([])
    const [error, setError] = useState('')

    useEffect( ()=>{
      const getUsuarios = async ()=>{
        const {docs} = await store.collection('agenda').get()
        const nuevoArray = docs.map(item => ({id:item.id, ...item.data()}))
        setUsuariosAgenda(nuevoArray)
      }

      getUsuarios()
    },[])
    
const setUsuarios = async (e) =>{
  e.preventDefault()
  if(!nombre.trim()){
    setError('El campo nombre esta vacio')
  }
  if(!phone.trim()){
    setError('El campo telefono esta vacio')
  }
  const usuario = {
    nombre:nombre,
    telefono:phone
  }
  try{
   
   const data= await store.collection('agenda').add(usuario)
   const {docs} = await store.collection('agenda').get()
   const nuevoArray = docs.map(item => ({id:item.id, ...item.data()}))
   setUsuariosAgenda(nuevoArray)
   alert('usuario nuevo')
    console.log('tarea añadida')

  }catch(e){
    console.log(e)
    console.log('Error')
  }
  setNombre('')
  setPhone('')
}
const BorrarUsusario = async (id) => {
  try{
    await store.collection('agenda').doc(id).delete()
    const {docs} = await store.collection('agenda').get()
    const nuevoArray = docs.map(item => ({id:item.id, ...item.data()}))
    setUsuariosAgenda(nuevoArray)
    alert('usuario eliminado')
  }catch(e){
    console.log(e)
  }
}
const PulsarActualizar = async (id)=>{
try{
  const data = await store.collection('agenda').doc(id).get()
  const { nombre , telefono} = data.data()
  setIdUsuario(id)
  setNombre(nombre)
  setPhone(telefono)
  setModoEdicion(true)
  console.log(data.data())

}catch(e){
  console.log(e)
}
}


const setUpdate = async (e) =>{
  e.preventDefault()
  if(!nombre.trim()){
    setError('El campo nombre esta vacio')
  }
  if(!phone.trim()){
    setError('El campo telefono esta vacio')
  }
  const userUpdate = {
    nombre:nombre,
    telefono:phone
  }
  try{
    await store.collection('agenda').doc(idusuario).set(userUpdate)
    const {docs} = await store.collection('agenda').get()
    const nuevoArray = docs.map(item => ({id:item.id, ...item.data()}))
    setUsuariosAgenda(nuevoArray)
    alert('usuario actualizado')
 
    console.log('modificado')

  }catch(e){
    console.log(e)
    console.log('Error')
  }
  setNombre('')
  setPhone('')
  setIdUsuario('')
  setModoEdicion(false)
}
  return (
    <div className="container">
     <div className="row">
       <div className="col">
          <h2>Formulario de usuarios</h2>
          <form onSubmit={modoedicion ? setUpdate : setUsuarios} className="form-group">
            <input
            value={nombre}
            onChange={(e)=>{
              setNombre(e.target.value)
            }}
            className="form-control"
            placeholder="Introduce el nombre"
            type="text"
            />
            <input
            value={phone}
            onChange={(e)=>{
              setPhone(e.target.value)
            }}
            className="form-control mt-3"
            placeholder="Introduce el telefono"
            type="text"
            />
            {
              modoedicion ?
              (
                <input  type="submit" value="Editar" className="btn btn-dark btn-block mt-3"/>
              )
              :
              (
                <input  type="submit" value="Registrar" className="btn btn-dark btn-block mt-3"/>
              )
            }
          
          </form>
          {
            error ?
            (
              <div>
                <p>
                  {error}                 
                </p>
              </div>
            )
            :
            (
              <span></span>
            )
          }
       </div>
       <div className="col">
          <h2>Lista de la Agenda</h2>
          <ul className="list-group">
          {
            usuariosAgenda.length !==0 ?
            (
             usuariosAgenda.map( item =>(
               <li className="list-group-item" key={item.id}>{item.nombre}-- {item.telefono}
               <button onClick={(id)=>{BorrarUsusario(item.id)}} className="btn btn-danger float-right">BORRAR</button>
               <button onClick={(id)=>{PulsarActualizar(item.id)}} className="btn btn-info mr-3 float-right">ACTUALIZAR</button>
               </li>
               
               ) )
            )
            :
            (
              <span>
                Aun no hay usuarios en tu agenda
              </span>
            )
          }
          </ul>
       </div>
       </div> 

    </div>
  );
}

export default App;
